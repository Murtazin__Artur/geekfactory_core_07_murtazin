package ru.murtazin.s02;

public class Task04 {

    public static void main(String[] args) {

        minFinder(15, 10);
    }

    public static void minFinder(int km, int futs) {
        if (km > (futs * 0.305)) {
            System.out.println(futs + " футов меньше чем " + km + " км");
        } else {
            System.out.println(futs + " футов больше чем " + km + " км");
        }
    }

}


