package ru.murtazin.s02;

public class Task05 {

    public static void main(String[] args) {
        whoseSquareLarget(5, 10);
    }

    public static void whoseSquareLarget(int quadrat, int circle) {
        if (Math.pow(quadrat, 2) > Math.PI * Math.pow(circle, 2)) {
            System.out.println("Площадь квадрата больше площади круга");
        } else {
            System.out.println("Площадь круга больше площади квадрата");
        }
    }
}
