package ru.murtazin.s02;

public class Task06 {

    public static void main(String[] args) {
        division(10, 5);
    }

    public static void division(int m, int n) {
        if ((m % n) > 0) {
            System.out.println(m + " / " + n + " Нацело не делится");
        } else {
            System.out.println(m + " / " + n + " =  " + m / n);
        }
    }
}

