package ru.murtazin.s02;

public class Task07 {

    public static void main(String[] args) {
        polindrom(125);
    }

    public static void polindrom(int count) {
        int firstDigit = (count / 100);
        int lastDigit = (count - 100) % 10;

        if (firstDigit == lastDigit) {
            System.out.println("Число " + count + " является полиндромом");
        } else {
            System.out.println("Число " + count + " не является полиндромом");
        }
    }
}
