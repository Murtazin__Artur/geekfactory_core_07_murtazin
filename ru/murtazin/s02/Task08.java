package ru.murtazin.s02;

public class Task08 {

    public static void main(String[] args) {
        sameNumbers(223);

    }

    public static void sameNumbers(int count) {
        int firstDigit = (count / 100);
        int secondDigit = ((count / 10) % 10);
        int ThirdDigit = (count - 100) % 10;

        if ((firstDigit == secondDigit) || (firstDigit == ThirdDigit) || (secondDigit == ThirdDigit)) {
            System.out.println("В числе " + count+ " есть одинаковые цифры");
        } else {
            System.out.println("В числе " + count + " нет одинаковых цифр");
        }
    }


}


