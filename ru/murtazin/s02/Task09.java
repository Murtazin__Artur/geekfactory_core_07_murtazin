package ru.murtazin.s02;

public class Task09 {

    public static void main(String[] args) {
               findY(-2);
    }

    public static void findY(double x) {
        if (x <= 0) {
            System.out.println("y = " + 0);
            return;
        }else if ((x > 0) && (x <= 1)) {
            System.out.println("y = " + x);
            return;
        } else {
            System.out.println("y = " + Math.pow(x, 2));
        }
    }
}
