package ru.murtazin.s02;

public class Task10 {

    public static void main(String[] args) {

        dayOfWeek(6);
    }

    public static void dayOfWeek(int x) {
        if (x == 1) {
            System.out.println("Понедельник");
            return;
        } else if (x == 2) {
            System.out.println("Вторник");
            return;
        } else if (x == 3) {
            System.out.println("Среда");
            return;
        } else if (x == 4) {
            System.out.println("Четверг");
            return;
        } else if (x == 5) {
            System.out.println("Пятница");
            return;
        } else if (x == 6) {
            System.out.println("Суббота");
            return;
        } else if (x == 7) {
            System.out.println("Воскресенье");
            return;
        } else {
            System.out.println("Такого дня недели не существует!!!");
        }

    }

}
