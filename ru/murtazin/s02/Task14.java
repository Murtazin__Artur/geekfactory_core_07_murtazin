package ru.murtazin.s02;

public class Task14 {

    public static void main(String[] args) {
        paintStars(5);
    }

    public static void paintStars(int count) {
        for (int i = 0; i < count; ++i) {
            for (int j = 0; j < i; ++j) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
