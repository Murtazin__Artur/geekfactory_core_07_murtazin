package ru.murtazin.s03;


public class Task01 {
    public static void main(String[] args) {
        insideTheWord("наоборот");

    }

    public static void insideTheWord(String word){
        char[] inside = word.toCharArray();
        for (int i = inside.length-1; i >= 0; i--) {
            System.out.print(inside[i]);
        }
        System.out.println();
    }
}
