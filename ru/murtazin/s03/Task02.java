package ru.murtazin.s03;

public class Task02 {
    public static void main(String[] args) {
        System.out.println(getStars("мир"));
    }

    public static String getStars(String word) {
        StringBuilder stars = new StringBuilder();

        for (int i = 0; i <= word.length() - 1; i++) {
            stars.append("*");
        }
        String result = stars + word + stars;
        return result;
    }
}
