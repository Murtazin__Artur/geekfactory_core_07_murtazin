package ru.murtazin.s03;

public class Task03 {

    public static void main(String[] args) {
        char simbol = 'о';
        String text = "Определить количество данного символа в тексте.";
        System.out.println(countChar(text, simbol));
    }

    public static int countChar(String text, char simbol) {
        int count = 0;

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == simbol) {
                count++;
            }
        }
        return count;
    }
}
