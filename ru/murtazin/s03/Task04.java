package ru.murtazin.s03;

public class Task04 {
    static String text;

    public static void main(String[] args) {
         text= "Эквивалентом изменяемой строки, являются StringBuffer StringBuilder. В многопоточной среде используем " +
                 "StringBuffer, в остальных случаях StringBuilder. Так как строки immutable, то при например, " +
                 "конкатенации, у нас будут создавать новые строки. Если у нас конкатенаций много, то лучше " +
                 "использовать StringBuffer StringBuilder. Что бы не было оверхеда по созданию большого кол-во " +
                 "объектов строк.";
        countChar();
    }

    public static void countChar() {
        int count = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == ' ') {
                count++;
            }
        }
        System.out.println("В этом тексте " + (count + 1) + " слов");
    }
}
