package ru.murtazin.s03;

public class Task05 {

    public static void main(String[] args) {
        if (istPalindrom("наган")) {
            System.out.println("Слово полиндром");
        } else {
            System.out.println("Слово не полиндром");
        }
    }

    public static boolean istPalindrom(String word) {
        boolean palindrom = false;
        if (word.length() % 2 == 0) {
            for (int i = 0; i < word.length() / 2 - 1; i++) {
                if (word.charAt(i) != word.charAt(word.length() - i - 1)) {
                    return false;
                } else {
                    palindrom = true;
                }
            }
        } else {
            for (int i = 0; i < (word.length() - 1) / 2 - 1; i++) {
                if (word.charAt(i) != word.charAt(word.length() - i - 1)) {
                    return false;
                } else {
                    palindrom = true;
                }
            }
        }
        return palindrom;
    }
}
