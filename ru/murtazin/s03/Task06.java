package ru.murtazin.s03;

public class Task06 {

    public static void main(String[] args) {
        getSumStringNumber("76995");
    }

    public static void getSumStringNumber(String word) {
        char[] digit = word.toCharArray();
        int sum = 0;

        for (int i = 0; i < digit.length; i++) {
            int num = Character.getNumericValue(digit[i]);
            sum = sum + num;
        }
        System.out.println(sum);
    }
}
