package ru.murtazin.s04;

public class House {
    private int lenght;
    private int width ;
    private int height;
    private int floors;
    private String name;

    House() {
    }

    House(String name, int lenght, int width, int height, int floors) {
        this.name = name;
        this.lenght = lenght;
        this.width = width;
        this.height = height;
        this.floors = floors;
    }

    public int getAreaHouse() {
        return lenght * width * floors;
    }

    public void sayArea() {
        System.out.println("Площадь  объекта " + getAreaHouse());
    }

    public int getAreaWalls() {
        return ((lenght * height * 2) + (lenght * width * 2));
    }

    public void sayAreaWalls() {
        System.out.println("Площадь стен " + getAreaWalls());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getFloors() {
        return floors;
    }

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public int getAreaAllWalls() {
        return (getAreaWalls() * floors);
    }

    @Override
    public String toString() {
        return "Строительство объекта " + name + " площадью " + getAreaHouse() + " кв.м." + " завершено!";
    }
}