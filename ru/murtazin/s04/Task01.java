package ru.murtazin.s04;

public class Task01 {

    public static void main(String[] args) {

        House saray = new House();
        saray.setName("Сарай");
        saray.setLenght(3);
        saray.setWidth(4);
        System.out.println("Создан объект " + saray.getName());
        saray.sayArea();
        System.out.println(saray.toString());
        System.out.println();

        House besedka = new House();
        besedka.setName("Беседка");
        besedka.setLenght(4);
        besedka.setHeight(2);
        besedka.setWidth(3);
        System.out.println("Создан объект " + besedka.getName());
        besedka.sayArea();
        besedka.sayAreaWalls();
        System.out.println(besedka.toString());
        System.out.println();

        House cottege = new House();
        cottege.setName("Коттедж");
        cottege.setLenght(6);
        cottege.setWidth(6);
        cottege.setHeight(2);
        cottege.setFloors(2);
        System.out.println("Создан объект " + cottege.getName());
        cottege.sayArea();
        cottege.sayAreaWalls();
        System.out.println(cottege.toString());
    }
}



