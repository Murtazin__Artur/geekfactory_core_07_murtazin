package ru.murtazin.s04;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Task02 {
    static String input;
    static int position;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите букву английского алфавита");
        input = in.next();
        Pattern pattern = Pattern.compile("[A-Za-z]");
        if ((pattern.matcher(input).find())) {

        } else {
            System.out.println("Введено некорректное значение!!!");
        }
        System.out.println("Вы ввели " + input + " позиция, введенной вами буквы, в алфавите " +
                getLetterPosition(input));
    }

    public static int getLetterPosition(String letter) {
        String letteris = letter;
        letteris = input.toUpperCase();
        position = Alphabet.valueOf(letteris).ordinal() + 1;
        return position;
    }

    public enum Alphabet {
        A, B, C, D, E, F, G, H, I, J,
        K, L, M, N, O, P, Q, R, S,
        T, U, V, W, X, Y, Z
    }
}

