package ru.murtazin.s06;

public class Task01 {

    public static void main(String[] args) {

        Segment Line1 = new Segment(1,1,2,2);
        System.out.println(Line1.toString());

        Segment Line2 = new Segment(-3,0,1,1);
        System.out.println(Line2.toString());

        if (Line1.lengthEquals(Line2)) {
            System.out.println("Длины отрезков равны");
        } else {
            if (Line1.lengthOfSegment() > Line2.lengthOfSegment()) {
                System.out.println("Первый отрезок длиннее второго");
            } else {
                System.out.println("Второй отрезок длиннее первого");
            }
        }
    }
}
