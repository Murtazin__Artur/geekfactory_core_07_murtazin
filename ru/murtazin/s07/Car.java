package ru.murtazin.s07;

import java.time.LocalDate;

public class Car {
    private String model;
    private String brand;
    private LocalDate realise;
    private String regNumber;
    private int mileage;

    public LocalDate getRealise() {
        return realise;
    }

    public void setRealise( LocalDate date) {
        this.realise = date;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    @Override
    public String toString() {
        return "Автомобиль: " + "Модель: " + model + " Марка " + brand + " Дата выпуска: " +
                realise + " Государственный регистрационный номер: " +
                regNumber + " пробег - " + mileage;
    }
}

