package ru.murtazin.s07;

public class CarAllowedValuesValidator implements Validator<Car> {
    private CarModelValidator valueModel = new CarModelValidator();
    private CarBrandValidator valueBrand = new CarBrandValidator();
    private CarRealiseValidator valueRealise = new CarRealiseValidator();
    private CarRegNumberValidator valueRegNumber = new CarRegNumberValidator();
    private CarMileageValidator valueMileage = new CarMileageValidator();

    @Override
    public boolean isValid(Car ob) {
        return ob != null && valueModel.isValid(ob.getModel()) && valueBrand.isValid(ob.getBrand())
                && valueRealise.isValid(ob.getRealise()) && valueRegNumber.isValid(ob.getRegNumber())
                && valueMileage.isValid(ob.getMileage());
    }
}
