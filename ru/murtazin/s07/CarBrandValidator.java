package ru.murtazin.s07;

public class CarBrandValidator implements Validator<String> {

    @Override
    public boolean isValid(String ob) {
        return ob != null && !ob.equals(" ") && !ob.equals("");
    }
}
