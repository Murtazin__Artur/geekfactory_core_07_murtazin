package ru.murtazin.s07;

public class CarConverter implements Converter<Car, CarExternalSystem> {

    @Override
    public CarExternalSystem toConvert(Car ob) {
        CarExternalSystem car = new CarExternalSystem();
        car.setModelAndBrand(ob.getModel() + " " + ob.getBrand());
        car.setDayRealise(ob.getRealise().getDayOfMonth());
        car.setMonthRealise(ob.getRealise().getMonthValue());
        car.setYearRealise(ob.getRealise().getYear());
        String regNumber = ob.getRegNumber().substring(0, ob.getRegNumber().length() - 6);
        String region = ob.getRegNumber().substring(ob.getRegNumber().length() - 6, ob.getRegNumber().length() - 3);
        car.setRegNumber(regNumber.trim());
        car.setRegionRegNumber(Integer.parseInt(region.trim()));
        car.setMileage(ob.getMileage());
        return car;
    }
}
