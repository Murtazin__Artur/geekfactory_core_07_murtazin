package ru.murtazin.s07;

public class CarExternalSystem {

    private String modelAndBrand;
    private int dayRealise;
    private int monthRealise;
    private int yearRealise;
    private String regNumber;
    private int regionRegNumber;
    private int mileage;

    public String getModelAndBrand() {
        return modelAndBrand;
    }

    public void setModelAndBrand(String modelAndBrand) {
        this.modelAndBrand = modelAndBrand;
    }

    public int getDayRealise() {
        return dayRealise;
    }

    public void setDayRealise(int dayRealise) {
        this.dayRealise = dayRealise;
    }

    public int getMonthRealise() {
        return monthRealise;
    }

    public void setMonthRealise(int monthRealise) {
        this.monthRealise = monthRealise;
    }

    public int getYearRealise() {
        return yearRealise;
    }

    public void setYearRealise(int yearRealise) {
        this.yearRealise = yearRealise;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public int getRegionRegNumber() {
        return regionRegNumber;
    }

    public void setRegionRegNumber(int regionRegNumber) {
        this.regionRegNumber = regionRegNumber;
    }

    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    @Override
    public String toString() {
        return "Автомобиль: " + modelAndBrand + " Дата выпуска: " + "День " + dayRealise + " Месяц " + monthRealise
                + " Год " + yearRealise + " Государственный регистрационный номер: " + regNumber +
                " регион регистрации - " + regionRegNumber + " пробег - " + mileage;
    }
}
