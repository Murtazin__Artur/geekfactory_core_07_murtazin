package ru.murtazin.s07;

public class CarMileageValidator implements Validator<Integer> {

    @Override
    public boolean isValid(Integer ob) {
        return ob > 0 && ob != null;
    }
}