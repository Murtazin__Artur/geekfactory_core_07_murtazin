package ru.murtazin.s07;

public class CarModelValidator implements Validator<String> {

    @Override
    public boolean isValid(String ob) {
        return ob != null && !ob.equals(" ") && !ob.equals("")
                && !ob.equals(ob.toLowerCase());
    }
}
