package ru.murtazin.s07;


public class CarRegNumberValidator implements Validator<String> {

    @Override
    public boolean isValid(String ob) {
        return ob != null && !ob.equals(" ") && !ob.equals("") && ob.length() >= 14 && ob.length() <= 15
                && ob.matches("^[А-Я]{1} [0-9]{3} [А-Я]{2} [0-9]{2,3}[RUS]{3}$");
    }
}