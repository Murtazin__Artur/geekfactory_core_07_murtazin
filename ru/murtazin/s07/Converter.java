package ru.murtazin.s07;

public interface Converter<T, S> {
    S toConvert(T ob);
}