package ru.murtazin.s07;

import java.time.LocalDate;

public class Driver {
    private String firstName;
    private String middleName;
    private String lastName;
    private LocalDate birthDay;
    private int driveExperience;
    private Car car;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public int getDriveExperience() {
        return driveExperience;
    }

    public void setDriveExperience(int driveExperience) {
        this.driveExperience = driveExperience;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Водитель: " + "Имя: " + firstName + " Отчество: " + middleName + " Фамилия: " + lastName +
                " дата рождения: " + birthDay + " Стаж вождения " + driveExperience + " лет." + " Личный транспорт: " +
                car.toString();
    }
}
