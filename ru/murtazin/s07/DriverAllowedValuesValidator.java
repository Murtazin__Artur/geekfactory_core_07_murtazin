package ru.murtazin.s07;

public class DriverAllowedValuesValidator implements Validator<Driver> {
    private DriverFirstNameValidator valueFirstName = new DriverFirstNameValidator();
    private DriverFirstNameValidator valueMiddleName = new DriverFirstNameValidator();
    private DriverFirstNameValidator valueLastName = new DriverFirstNameValidator();
    private DriverBirthDayValidator valueBirthDay = new DriverBirthDayValidator();
    private DriverDriveExperianceValidator valueDriveExperiance = new DriverDriveExperianceValidator();
    private CarAllowedValuesValidator valueCar = new CarAllowedValuesValidator();

    @Override
    public boolean isValid(Driver ob) {
        return !(ob == null) && valueFirstName.isValid(ob.getFirstName()) && valueMiddleName.isValid(ob.getMiddleName())
                && valueLastName.isValid(ob.getLastName()) && valueBirthDay.isValid(ob.getBirthDay())
                && valueDriveExperiance.isValid(ob.getDriveExperience()) && valueCar.isValid(ob.getCar());
    }
}
