package ru.murtazin.s07;

import java.time.LocalDate;

public class DriverBirthDayValidator implements Validator<LocalDate> {

    @Override
    public boolean isValid(LocalDate ob) {
        return ob != null && !ob.equals(" ") && !ob.equals("") && ob.toString().length() <= 10
                && ob.toString().length() >= 10 && (LocalDate.now().getYear() - ob.getYear()) >= 18
                && (LocalDate.now().getYear() - ob.getYear()) <= 120;
    }
}
