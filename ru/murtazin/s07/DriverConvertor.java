package ru.murtazin.s07;

public class DriverConvertor implements Converter<Driver, DriverExternalSystem> {

    @Override
    public DriverExternalSystem toConvert(Driver ob) {
        DriverExternalSystem car = new DriverExternalSystem();
        car.setFamiliaImiaOtchestvo(ob.getFirstName() + " " + ob.getMiddleName() + " " + ob.getLastName());
        car.setDayBirthDay(ob.getBirthDay().getDayOfMonth());
        car.setMonthBirthDay(ob.getBirthDay().getMonthValue());
        car.setYearBirthDay(ob.getBirthDay().getYear());
        car.setDriveExperiance(ob.getDriveExperience() * 12);
        return car;
    }
}
