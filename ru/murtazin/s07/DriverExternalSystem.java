package ru.murtazin.s07;

public class DriverExternalSystem {
    private String familiaImiaOtchestvo;
    private int dayBirthDay;
    private int monthBirthDay;
    private int yearBirthDay;
    private int driveExperiance;

    public String getFamiliaImiaOtchestvo() {
        return familiaImiaOtchestvo;
    }

    public void setFamiliaImiaOtchestvo(String familiaImiaOtchestvo) {
        this.familiaImiaOtchestvo = familiaImiaOtchestvo;
    }

    public int getDayBirthDay() {
        return dayBirthDay;
    }

    public void setDayBirthDay(int dayBirthDay) {
        this.dayBirthDay = dayBirthDay;
    }

    public int getMonthBirthDay() {
        return monthBirthDay;
    }

    public void setMonthBirthDay(int monthBirthDay) {
        this.monthBirthDay = monthBirthDay;
    }

    public int getYearBirthDay() {
        return yearBirthDay;
    }

    public void setYearBirthDay(int yearBirthDay) {
        this.yearBirthDay = yearBirthDay;
    }

    public int getDriveExperiance() {
        return driveExperiance;
    }

    public void setDriveExperiance(int driveExperiance) {
        this.driveExperiance = driveExperiance;
    }

    @Override
    public String toString() {
        return "Водитель: " + familiaImiaOtchestvo + " дата рождения: " + "День: " + dayBirthDay + " Месяц: " + monthBirthDay +
                " Год: " + yearBirthDay + " Стаж вождения " + driveExperiance + " месяцев.";
    }
}
