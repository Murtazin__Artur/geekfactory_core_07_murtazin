package ru.murtazin.s07;

public class DriverFirstNameValidator implements Validator<String> {

    @Override
    public boolean isValid(String ob) {
        return ob != null && !ob.equals(" ") && !ob.equals("") && ob.length() >= 2
                && ob.matches("^[А-Я]{1}[а-я]{1,14}$");
    }
}