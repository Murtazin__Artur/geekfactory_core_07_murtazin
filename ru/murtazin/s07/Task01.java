package ru.murtazin.s07;

import java.time.LocalDate;

public class Task01 {

    public static void main(String[] args) {
        CarAllowedValuesValidator valuecar = new CarAllowedValuesValidator();
        Car kopeika = new Car();
        String brand = "2101";
        CarBrandValidator valueBrand = new CarBrandValidator();
        if (!valueBrand.isValid(brand)) {
            System.out.println("Вы ввели некорректное значение в поле Марка автомобиля!");
        } else {
            kopeika.setBrand(brand);
        }
        String model = "ВАЗ";
        CarModelValidator valueModel = new CarModelValidator();
        if (!valueModel.isValid(model)) {
            System.out.println("Вы ввели некорректное значение в поле Модель!");
        } else {
            kopeika.setModel(model);
        }
        int miliage = 30000;
        CarMileageValidator valueMileage = new CarMileageValidator();
        if (!valueMileage.isValid(miliage)) {
            System.out.println("Вы ввели некорректное значение в поле Пробег!");
        } else {
            kopeika.setMileage(miliage);
        }
        LocalDate realise = LocalDate.of(1985, 11, 8);
        CarRealiseValidator valueRealise = new CarRealiseValidator();
        if (!valueRealise.isValid(realise)) {
            System.out.println("Вы ввели некорректное значение в поле Дата выпуска!");
        } else {
            kopeika.setRealise(realise);
        }
        String regNumber = "М 777 АИ 40RUS";
        CarRegNumberValidator valueRegNumber = new CarRegNumberValidator();
        if (!valueRegNumber.isValid(regNumber)) {
            System.out.println("Вы ввели некорректное значение в поле Регистрационный номер! ");
        } else {
            kopeika.setRegNumber(regNumber);
        }
        if (!valuecar.isValid(kopeika)) {
            System.out.println("Поля введены не корректно Объект не может быть создан!");
        } else {
            System.out.println("Поля введены корректно. Объект был успешно создан!");
            System.out.println(kopeika.toString());
            System.out.println();
        }

        DriverAllowedValuesValidator valueDriver = new DriverAllowedValuesValidator();
        Driver racer = new Driver();
        CarAllowedValuesValidator valueDriverCar = new CarAllowedValuesValidator();
        if (!valueDriverCar.isValid(kopeika)) {
            System.out.println("Поля введены не корректно Объект не может быть использован! " +
                    "Исправьте ошибки и повторите снова!");
        } else {
            racer.setCar(kopeika);
            System.out.println("Поля введены корректно. Автомобиль был успешно привязан к водителю!");
        }
        String firstName = "Иван";
        DriverFirstNameValidator valueFirstName = new DriverFirstNameValidator();
        if (!valueFirstName.isValid(firstName)) {
            System.out.println("Вы ввели некорректное значение в поле Имя!");
        } else {
            racer.setFirstName(firstName);
        }
        String middleName = "Акакиевич";
        DriverFirstNameValidator valueMiddleName = new DriverFirstNameValidator();
        if (!valueMiddleName.isValid(middleName)) {
            System.out.println("Вы ввели некорректное значение в поле Отчество!");
        } else {
            racer.setMiddleName(middleName);
        }
        String lastName = "Зелипукин";
        DriverFirstNameValidator valueLastName = new DriverFirstNameValidator();
        if (!valueLastName.isValid(lastName)) {
            System.out.println("Вы ввели некорректное значение в поле Фамилия!");
        } else {
            racer.setLastName(lastName);
        }
        LocalDate birthDay = LocalDate.of(1985, 11, 8);
        DriverBirthDayValidator valueBirthDay = new DriverBirthDayValidator();
        if (!valueBirthDay.isValid(birthDay)) {
            System.out.println("Вы ввели некорректное значение в поле Дата рождения!");
        } else {
            racer.setBirthDay(birthDay);
        }
        int driveExperience = 5;
        DriverDriveExperianceValidator valueDriveExperience = new DriverDriveExperianceValidator();
        if (!valueDriveExperience.isValid(driveExperience)) {
            System.out.println("Вы ввели некорректное значение в поле Пробег!");
        } else {
            racer.setDriveExperience(driveExperience);
        }
        if (!valueDriver.isValid(racer)) {
            System.out.println("Поля введены не корректно Объект не может быть создан!");
        } else {
            System.out.println("Поля введены корректно. Объект был успешно создан!");
            System.out.println(racer.toString());
        }
    }
}


