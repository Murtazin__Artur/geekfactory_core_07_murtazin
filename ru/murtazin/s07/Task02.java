package ru.murtazin.s07;

import java.time.LocalDate;

public class Task02 {

    public static void main(String[] args) {
        Car kopeika = new Car();
        CarConverter carConverter = new CarConverter();
        kopeika.setBrand("2101");
        kopeika.setModel("ВАЗ");
        kopeika.setMileage(300000);
        LocalDate realise = LocalDate.of(1985, 11, 8);
        kopeika.setRealise(realise);
        kopeika.setRegNumber("М 777 АИ 40RUS");
        System.out.println(kopeika.toString());
        System.out.println(carConverter.toConvert(kopeika).toString());
        System.out.println();

        Driver racer = new Driver();
        DriverConvertor driverConvertor = new DriverConvertor();
        racer.setCar(kopeika);
        racer.setFirstName("Иван");
        racer.setMiddleName("Акакиевич");
        racer.setLastName("Зелипукин");
        LocalDate birthDay = LocalDate.of(1985, 11, 8);
        racer.setBirthDay(birthDay);
        racer.setDriveExperience(5);
        System.out.println(racer.toString());
        System.out.println(driverConvertor.toConvert(racer).toString());
    }
}
