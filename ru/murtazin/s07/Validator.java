package ru.murtazin.s07;

public interface Validator<T> {
    boolean isValid(T ob);
}

