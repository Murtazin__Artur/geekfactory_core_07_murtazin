package ru.murtazin.s08;

    class BadFileFormatException extends Exception {

        private String format;

        public BadFileFormatException(String message, String formate) {

            System.out.println(message);
            format = formate;
        }

        public BadFileFormatException(String message, Throwable cause, String format) {
            super(message, cause);
            this.format = format;
        }

        public BadFileFormatException(Throwable cause, String format) {
            super(cause);
            this.format = format;
        }

        public BadFileFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String format) {
            super(message, cause, enableSuppression, writableStackTrace);
            this.format = format;
        }

        public BadFileFormatException(String format) {
            this.format = format;
        }

        public String getFormat() {
            return format;
        }
    }
