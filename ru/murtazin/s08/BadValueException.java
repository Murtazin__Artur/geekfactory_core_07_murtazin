package ru.murtazin.s08;

class BadValueException extends Exception {
    public BadValueException(String value) {
        this.value = value;
    }

    public BadValueException(String message, String value) {
        super(message);
        this.value = value;
    }

    public BadValueException(String message, Throwable cause, String value) {
        super(message, cause);
        this.value = value;
    }

    public BadValueException(Throwable cause, String value) {
        super(cause);
        this.value = value;
    }

    public BadValueException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, String value) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.value = value;
    }

    private String value;

        public BadValueException(String message, String values, Throwable cause) {

            System.out.println(message);
            value = values;
        }

        public String getValue() {
            return value;
        }
    }



