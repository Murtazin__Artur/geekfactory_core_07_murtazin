package ru.murtazin.s08;

import java.io.File;
import java.math.BigInteger;

public class Calculator {

    static void sumAllNumbersOfTheString(String inputfile, String outputfile, String fileformat) throws Exception {
        BigInteger sum = BigInteger.valueOf(0);
        String numbers = ReadAndWriteFile.readFirstLine(new File(inputfile + fileformat));
        if (numbers == null) {
            throw new BadValueException("Файл пустой. Произвести раcчет невозможно!!!", "null");
        }
        if (!(numbers.matches("[\\d\\s]+"))) {
            throw new BadValueException("В строке присутствуют буквы. Произвести рачет невозможно!!!", "null");
        }
        String[] element = numbers.split(" ");
        for (int i = 0; i <= element.length - 1; i++) {
            sum = sum.add(new BigInteger(element[i]));
            if (sum.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) >= 0) {
                throw new BadValueException("сумма превышает допустимое значение Long.MAX_VALUE", "null");
            }
            ReadAndWriteFile.writeToFirst(new File(outputfile + fileformat), String.valueOf(sum));
        }
    }
}