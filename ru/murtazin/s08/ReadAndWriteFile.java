package ru.murtazin.s08;

import java.io.*;
import java.nio.channels.FileLockInterruptionException;

public class ReadAndWriteFile {
    private static String first;

    static String readFirstLine(File file) throws Exception {
        FileInputStream stream = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        if (!(file.exists())) {
            throw new FileNotFoundException();
        }
        if (!(file.getName().substring(file.getName().lastIndexOf(".")).equals(".txt"))) {
            throw new BadFileFormatException("Неверный формат файла. Выберите файл с расширением .txt", ".txt");
        }
        if (!(file.canRead())) {
            throw new FileLockInterruptionException();
        } else {
            first = reader.readLine();
            reader.close();
            stream.close();
            System.out.println("Чтение из файла прошла успешно!!!");
        }
        return first;
    }

    static void writeToFirst(File file, String value) throws Exception {
        FileWriter writer = new FileWriter(file, false);
        if (!(file.exists())) {
            throw new FileNotFoundException();
        }
        if (!(file.getName().substring(file.getName().lastIndexOf(".")).equals(".txt"))) {
            throw new BadFileFormatException("Неверный формат файла. Выберите файл с расширением .txt", ".txt");
        }
        if (value == null) {
            throw new BadValueException("Нет данных для записи", "null");
        }
        if (!(file.canWrite())) {
            throw new FileLockInterruptionException();
        }
        writer.write(value);
        writer.flush();
        writer.close();
        System.out.println("Запись в файл прошла успешно!!!");
    }
}