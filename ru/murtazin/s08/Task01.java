package ru.murtazin.s08;

import java.io.FileNotFoundException;
import java.nio.channels.FileLockInterruptionException;

public class Task01 {


    public static void main(String[] args) throws Exception {

        try {
            Calculator.sumAllNumbersOfTheString("C:\\MyJava\\arguments", "C:\\MyJava\\result", ".txt");
        } catch (BadValueException e) {
            System.err.println(e.getMessage());
        } catch (BadFileFormatException e) {
            System.err.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (FileLockInterruptionException e) {
            System.err.println(e.getMessage());
        }
    }
}