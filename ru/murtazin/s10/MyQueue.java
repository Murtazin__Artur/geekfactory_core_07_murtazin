package ru.murtazin.s10;

import java.util.LinkedList;
import java.util.List;

public class MyQueue<T> implements Queue<T> {
    private List<T> linkedList = new LinkedList();

    @Override
    public boolean add(T e) {
        linkedList.add(e);
        return false;
    }

    @Override
    public T poll() {
        if (linkedList.size() == 0 || linkedList.get(0) == null) {
            return null;
        } else
            return linkedList.remove(0);
    }

    @Override
    public T peek() {
        T firstElement = linkedList.get(0);
        return firstElement;
    }

    @Override
    public String toString() {
        return "MyQueue " + linkedList;
    }
}
