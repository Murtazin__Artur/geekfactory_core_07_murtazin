package ru.murtazin.s10;

import java.util.LinkedList;

public class Task05 {

    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();
        addMyQueue(myQueue, 9);
        removeMyQueue(myQueue);
        long time = System.nanoTime();
        time = System.nanoTime() - time;
        System.out.printf("Время выполнения: %,9.3f ms\n", time / 1_000_000.0);

        addJDKQueue(9);
        long jdkTime = System.nanoTime();
        removeJDKQueue(9);
        jdkTime = System.nanoTime() - jdkTime;
        System.out.printf("Время выполнения: %,9.3f ms\n", jdkTime / 1_000_000.0);
    }

    public static void addMyQueue(MyQueue myQueueQ, int countElement) {
        for (int i = 0; i <= countElement; i++) {
            myQueueQ.add(i);
        }
        System.out.println("Было: " + myQueueQ.toString());
    }

    public static void addJDKQueue(int countElement) {
        java.util.Queue jdkQueue = new LinkedList();
        for (int i = 0; i <= countElement; i++) {
            jdkQueue.add(i);
        }
    }

    public static void removeJDKQueue(int countElement) {
        java.util.Queue jdkQueue = new LinkedList();
        if (jdkQueue.peek() == null) {
            System.out.println("Очередь пуста");
        } else {
            jdkQueue.poll();
        }
    }

    public static void removeMyQueue(MyQueue myQueueQ) {
        if (myQueueQ == null) {
            System.out.println("Очередь пуста");
        } else {
            System.out.println(myQueueQ.poll() + " Извлечена из очереди");
        }
        System.out.println("Стало: " + myQueueQ.toString());
    }
}