package ru.murtazin.s12;

public class BubbleSort {

    public static void bubbleSort(int[] intArray) {
        for (int i = intArray.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (intArray[j] > intArray[j + 1]) {
                    int greater = intArray[j];
                    intArray[j] = intArray[j + 1];
                    intArray[j + 1] = greater;
                }
            }
        }
    }
}