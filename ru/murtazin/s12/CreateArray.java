package ru.murtazin.s12;

public class CreateArray {

    public static int[] createArray(int arrayLength) {
        int[] intArray;
        intArray = new int[arrayLength];
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = ((int) (Math.random() * arrayLength));
        }
        System.out.println("Исходный массив:");
        for (int i = 0; i <= intArray.length - 1; i++) {
            System.out.print(intArray[i] + " ");
        }
        System.out.println();
        return intArray;
    }
}
