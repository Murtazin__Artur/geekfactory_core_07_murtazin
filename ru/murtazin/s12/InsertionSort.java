package ru.murtazin.s12;

public class InsertionSort {

    public static void insertionSort(int [] intArray) {
        for (int i = 1; i < intArray.length; i++) {
            int arrayElement = intArray[i];
            int count = i - 1;
            while (count >= 0 && intArray[count] > arrayElement) {
                intArray[count + 1] = intArray[count];
                intArray[count] = arrayElement;
                count--;
            }
        }
    }
}
