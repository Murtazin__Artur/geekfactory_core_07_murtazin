package ru.murtazin.s12;

import java.util.Arrays;

public class MergeSort {

    public static int[] sorting(int[] intArray) {
        if (intArray.length < 2) {
            return intArray;
        }
        int delimiter = intArray.length / 2;
        int[] intArray1 = Arrays.copyOfRange(intArray, 0, delimiter);
        int[] intArray2 = Arrays.copyOfRange(intArray, delimiter, intArray.length);
        int [] result = merge(sorting(intArray1), sorting(intArray2));
        return result;
    }

    private static int[] merge(int[] intArray1, int[] intArray2) {
        int n = intArray1.length + intArray2.length;
        int[] intArray = new int[n];
        int indexFirstArray = 0;
        int indexSecondArray = 0;
        for (int i = 0; i < n; i++) {
            if (indexFirstArray== intArray1.length) {
                intArray[i] = intArray2[indexSecondArray];
            } else if (indexSecondArray == intArray2.length) {
                intArray[i] = intArray1[indexFirstArray];
            } else {
                if (intArray1[indexFirstArray] < intArray2[indexSecondArray]) {
                    intArray[i] = intArray1[indexFirstArray++];
                } else {
                    intArray[i] = intArray2[indexSecondArray++];
                }
            }
        }
        return intArray;
    }
}