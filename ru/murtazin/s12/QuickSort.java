package ru.murtazin.s12;

public class QuickSort {

    public static void quickSort(int[] intArray, int firstElement, int lastElement) {
        if (intArray.length == 0) {
            System.err.println("Массив является пустым!");
        }
        if (firstElement >= lastElement) {
            System.err.println("Выполнить сортировку невозможно. Индекс последнего элемента массива меньше первого");
        }
        int middle = firstElement + (lastElement - firstElement) / 2;
        int currentElement = intArray[middle];
        int firstValue = firstElement;
        int lastValue = lastElement;
        while (firstValue <= lastValue) {
            while (intArray[firstValue] < currentElement) {
                firstValue++;
            }
            while (intArray[lastValue] > currentElement) {
                lastValue--;
            }
            if (firstValue <= lastValue) {
                int value = intArray[firstValue];
                intArray[firstValue] = intArray[lastValue];
                intArray[lastValue] = value;
                firstValue++;
                lastValue--;
            }
        }
        if (firstElement < lastValue) {
            quickSort(intArray, firstElement, lastValue);
        }
        if (lastElement > firstValue) {
            quickSort(intArray, firstValue, lastElement);
        }
    }
}
