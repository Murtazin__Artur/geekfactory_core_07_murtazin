package ru.murtazin.s12;

public class SelectionSort {

    public static void selectionSort(int[] intArray) {
        for (int i = 0; i < intArray.length; i++) {
            int minArray = intArray[i];
            int minCount = i;
            for (int j = i + 1; j < intArray.length; j++) {
                if (intArray[j] < minArray) {
                    minArray = intArray[j];
                    minCount = j;
                }
            }
            if (i != minCount) {
                int greater = intArray[i];
                intArray[i] = intArray[minCount];
                intArray[minCount] = greater;
            }
        }
    }
}
