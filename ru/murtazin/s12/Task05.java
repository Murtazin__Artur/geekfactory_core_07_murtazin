package ru.murtazin.s12;

public class Task05 {

    public static void main(String[] args) {
        int[] intArray = CreateArray.createArray(10);
        int[] newArray = MergeSort.sorting(intArray);
        System.out.println("Массив отсортирован алгоритмом  Merge Sort");
        for (int i = 0; i <= newArray.length - 1; i++) {
            System.out.print(newArray[i] + " ");
        }
        System.out.println();
    }
}
