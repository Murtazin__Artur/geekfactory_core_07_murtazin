package ru.murtazin.s12;

import java.util.Arrays;

public class Task06 {

    public static void main(String[] args) {
        int[] intArray = CreateArray.createIntArray(10);
        System.out.println("Исходный массив: " + Arrays.toString(intArray));
        QuickSort.quickSort(intArray, 0, intArray.length - 1);
        System.out.println("Массив отсортирован методом Quick Sort: " + Arrays.toString(intArray));
    }
}
